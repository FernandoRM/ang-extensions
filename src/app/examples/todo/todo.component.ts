import { Component } from '@angular/core';
import { TodoService } from './todo.service';

@Component({
  selector: 'app-todo',
  template: // html
  `
    <ng-container *ngIf="tdSrv.todos$ | async as todos">
      <h1>Todo List | {{todos.length}}</h1>
      <ul>
        <li class="todo" *ngFor="let todo of todos">
          <span (click)="toggleTodo(todo.id)" [ngClass]="{ done: todo.done }">
            {{ todo.name }} <button (click)="removeTodo(todo.id)">x</button>
          </span>
        </li>
      </ul>
      <div>
        <input type="text" #newTodoName> 
        <button (click)="addTodo(newTodoName.value);newTodoName.value=''">
          Add todo
        </button>      
      </div>
      <p>Click on todo to toggle its state</p> 
      <a routerLink='/'> Toogle to index</a>
    </ng-container>
  `,
  styleUrls: ['./todo.component.sass']
})
export class TodoComponent {

  constructor( public tdSrv: TodoService) { }

  addTodo(name: string) {
    name.length ? this.tdSrv.addTodo(name) : alert('Please, type something to add some task to do');
  }

  removeTodo(id: number) {
    this.tdSrv.removeTodo(id);
  }

  toggleTodo(id: number) {
    this.tdSrv.toggleTodo(id);
  }

}
